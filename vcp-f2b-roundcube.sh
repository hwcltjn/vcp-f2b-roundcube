#!/bin/bash

# Script that adds fail2ban protection for RoundCube on Vesta CP.

# Made by hwcltjn - http://hwclondon.com
# https://gitlab.com/hwcltjn/vcp-f2b-roundcube
# Last Update: 10/10/2017

#---- Variables ----#
vestaHostname=$(hostname -f)
config_fail2ban="/etc/fail2ban/jail.local"

# If you need to manually override running the script as root. But why?
root_run="true"
# If you need to manually override running on Ubuntu.
os_override="false"

#---- Root user check ----#
if [ "$root_run" = "true" ] && [ "$EUID" -ne 0 ]; then 
	echo "ERROR: Please run script as root."
	exit 1
fi

#---- Counter -----#
# I'm just genuinely curious as to how many times my script is run :)
# You, your IP, your hardware, your location or any other analytics are NOT being logged/tracked - only how many times the link is hit.
# If you are also curious as to how many times this script has been run, please visit: http://hwcl.net/counter/
curl -s -m 5 https://hwcl.net/counter/count_f2b_rcube.php >/dev/null

#---- Check for Ubuntu ----#
os_check=$(head -n1 /etc/issue | cut -f 1 -d ' ')
if [ "$os_check" != "Ubuntu" ] && [ "$os_override" = "false" ]; then
	echo "ERROR: It doesn't look like your are running Ubuntu. Exiting."
	exit 1
fi

command -v apache2 >/dev/null 2>&1 || { echo "ERROR: Can't find apache2. Exiting." && exit 1 ; }

if [ ! -f /usr/local/vesta/bin/v-add-firewall-chain ]; then
	echo "Error: Vesta Firewall function seems to be missing?. Exiting."
	exit 1
fi

if [ -f /usr/share/roundcube/index.php ] && [ -d /var/log/roundcube ]; then
	touch /var/log/roundcube/errors
	chown -R admin:admin /var/log/roundcube
	wget https://gitlab.com/hwcltjn/vcp-f2b-roundcube/raw/master/vcp-roundcube.conf -O /etc/fail2ban/filter.d/vcp-roundcube.conf
	wget https://gitlab.com/hwcltjn/vcp-f2b-roundcube/raw/master/vcp-a-roundcube.conf -O /etc/fail2ban/action.d/vcp-roundcube.conf

	if [ -f $config_fail2ban ]; then
		echo "" >> $config_fail2ban
		echo "[roundcube]" >> $config_fail2ban
		echo "enabled = true" >> $config_fail2ban
		echo "filter = vcp-roundcube" >> $config_fail2ban
		echo "action = vcp-roundcube" >> $config_fail2ban
		echo "logpath = /var/log/roundcube/errors" >> $config_fail2ban
		echo "findtime = 600" >> $config_fail2ban
		echo "maxretry = 5" >> $config_fail2ban
		echo "bantime = 900" >> $config_fail2ban

		service fail2ban restart
	    if [ "$?" = "0" ]; then
	    	sleep 2
	    	echo "Fail2ban restarted successfully."
	    	echo "RoundCube should now be protected!"
	    else
	    	echo "Error: Could not restart fail2ban - something went wrong."
	    fi

	else
		echo "Error: Could not find $config_fail2ban"
	fi

else
	echo "Is roundcube installed? Exiting."
	exit 1
fi

exit 0