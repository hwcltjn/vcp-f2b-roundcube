# Vesta CP Fail2Ban jail for RoundCube

This script adds fail2ban protection for RoundCube in VestaCP.

#### What this script does:
1 - Applies the correct permissions to the `/var/log/roundcube/` folder.

2 - Downloads and configures a fail2ban [filter](https://gitlab.com/hwcltjn/vcp-f2b-roundcube/blob/master/vcp-roundcube.conf) and [action](https://gitlab.com/hwcltjn/vcp-f2b-roundcube/blob/master/vcp-a-roundcube.conf) to monitor the roundcube error log for failed logins.

#### Requirements
This modification requires - and has only been tested on:
- Vesta CP 0.9.8-17 running **Apache2 + NGINX**
- Ubuntu 16.0.4
- RoundCube

A version that works on nginx+php-fpm and other distributions should follow.

# Usage
**Use as is - I take no responsability if this damages your server.**

Run the command below as root to download and run the script:
```
wget https://gitlab.com/hwcltjn/vcp-f2b-roundcube/raw/master/vcp-f2b-roundcube.sh && bash vcp-f2b-roundcube.sh

```